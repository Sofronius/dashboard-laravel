@extends('adminku.admin')
  
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Anda Sedang Menambahkan</h2>
        </div>
    </div>
</div>

<hr class="my-4">
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Heyyy</strong>isi sek........<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


   
<form action="{{ route('adminku.store') }}" method="POST">
   <input type="hidden" name="_token" value="{{ csrf_token() }}">
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                <input type="text" name="name" class="form-control" placeholder="Name">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Detail:</strong>
                <textarea class="form-control" style="height:150px" name="detail" placeholder="Detail"></textarea>
            </div>
        </div>
        <div>
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
         <div>
            <a class="btn btn-danger" href="{{ route('adminku.admin') }}">Batal</a>
        </div>
    </div>
   
</form>
@endsection