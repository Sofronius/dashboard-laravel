@extends('products.admin')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('products.create') }}">
                    <i class="fas fa-plus"></i> Tambah Post</a>
            </div>
            <hr class="my-4">
        </div>
   
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
   <!--  <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Details</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($products as $product)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $product->name }}</td>
            <td>{{ $product->detail }}</td>
            <td>
                <form action="{{ route('products.destroy',$product->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('products.show',$product->id) }}">Show</a>
    
                    <a class="btn btn-warning" href="{{ route('products.edit',$product->id) }}">Edit</a>

                    {{ method_field('DELETE') }}
                    <button type="submit" class="btn btn-danger"  name="_token" value="{{ csrf_token() }}">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table> -->

         @foreach ($products as $product)
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <p class="lead">{{ ++$i }}</p>
                    <div class="card-title">{{ $product->name }}</div>
                    <p class="card-text"></p>
                    <form action="{{ route('products.destroy',$product->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('products.show',$product->id) }}">Preview</a>
    
                    <a class="btn btn-warning text-white" href="{{ route('products.edit',$product->id) }}">Edit</a>

                    {{ method_field('DELETE') }}
                    <button type="submit" class="btn btn-danger"  name="_token" value="{{ csrf_token() }}">Delete</button>
                </form>
                </div>
            </div>
        </div>
         @endforeach
     </div>
  
    {!! $products->links() !!}
      
@endsection