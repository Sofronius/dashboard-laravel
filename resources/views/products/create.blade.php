@extends('products.admin')
  
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h4 class="display-5 ml-5">Tambahkan Pos Baru</h4>
        </div>
    </div>
</div>

<hr class="my-4">
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Heyyy</strong>isi sek........<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


   
<form action="{{ route('products.store') }}" method="POST">
   <input type="hidden" name="_token" value="{{ csrf_token() }}">
     <div class="row">
        <div class="col-xs-4 col-sm-4 col-md-4 ml-5">
            <div class="form-group">
                <strong>Judul :</strong>
                <input type="text" name="name" class="form-control mt-3" placeholder="Tambahkan Judul">
            </div>
        </div>
        <div class="col-xs-5 col-sm-5 col-md-8 ml-5 mb-4">
            <div class="form-group">
                <strong>Sinopsis :</strong>
                <textarea class="form-control mt-3" style="height:150px" name="detail"></textarea>
            </div>
        </div>
        <div class="col-xs-5 col-sm-5 ml-5">
            <button type="submit" class="btn btn-primary">Publish</button>
            <a class="btn btn-danger" href="{{ route('products.index') }}">Batal</a>
         </div>   
    </div>
   
</form>
@endsection