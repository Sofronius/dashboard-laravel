@extends('products.admin')
@section('content')
    <!-- <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Preview</h2>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Judul :</strong>
                {{ $product->name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Sinopsis :</strong>
                {{ $product->detail }}
            </div>
        </div>
        <div class="pull-right">
                <a class="btn btn-danger" href="{{ route('products.index') }}">Exit</a>
            </div>
    </div> -->

   <div class="jumbotron jumbotron-fluid col-xs-11 col-sm-11 col-md-11 ml-5">
      <div class="container ml-3">
        <p class="lead">{{ $product->name }}</p>
        <p class="lead">{{ $product->detail }}</p>
        <a class="btn btn-danger" href="{{ route('products.index') }}">Exit</a>
      </div>
    </div>
@endsection