@extends('products.admin')
   
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3 class="display-5 ml-5">Edit Post</h3>
            </div>
            
        </div>
    </div>

  <hr class="my-4">  
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>ooo!</strong>Kamu Belum Menambahkan nya<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

<div class="row">
    <div class="col-md-10 ml-5">
    <form action="{{ route('products.update',$product->id) }}"  method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        {{ method_field('PATCH') }}
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Judul :</strong>
                    <input type="text" name="name" value="{{ $product->name }}" class="form-control mt-3" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group mb-4">
                    <strong>Sinopsis :</strong>
                    <textarea class="form-control mt-3" style="height:150px" name="detail" placeholder="Detail">{{ $product->detail }}</textarea>
                </div>
            </div>
              <button type="submit" class="btn btn-primary">Perbarui</button>
              <a class="btn btn-danger" href="{{ route('products.index') }}">Batal</a>
        </form>
    </div>
</div> 

@endsection

